______               _  ___  ___     _ 
| ___ \             | | |  \/  |    | |
| |_/ /___  __ _  __| | | .  . | ___| |
|    // _ \/ _` |/ _` | | |\/| |/ _ \ |
| |\ \  __/ (_| | (_| | | |  | |  __/_|
\_| \_\___|\__,_|\__,_| \_|  |_/\___(_)
                                       
                                       
-- USAGE --
- Use "node app" in command prompt to run server
- Type port number when prompted and press enter to start

- For client, compile project and run .exe file, lobby displays in command prompt

-- CONTROLS --
WASD + Arrow Keys : Movement
ESCAPE            : Close the client game window
ENTER/RETURN      : Move to random location
Number keys	  : Choose game to join (0 to make new game)