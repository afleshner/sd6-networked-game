﻿//---------------------------------------------------------------------------
// PacketParser.js
//---------------------------------------------------------------------------

module.exports =
{
    ConstructPositionPacket: function ( channelID, packetID, playerID, rgba, position, velocity, turretDegrees )
    {
        return ConstructPositionPacket( channelID, packetID, playerID, rgba, position, velocity, turretDegrees );
    },
    ConstructShotPacket: function( channelID, packetID, shootingPlayerID, didHit, hitPlayerID )
    {
        return ConstructShotPacket( channelID, packetID, shootingPlayerID, didHit, hitPlayerID );
    },
    ConstructAckPacket: function ( channelID, packetID, playerID )
    {
        return ConstructAckPacket( channelID, packetID, playerID );
    },
    ConstructLobbyListPacket: function( channelID, packetID, numRooms )
    {
        return ConstructLobbyListPacket( channelID, packetID, numRooms );
    },
    ConstructEndGamePacket: function( channelID, packetID )
    {
        return ConstructEndGamePacket( channelID, packetID );
    },
    ParsePacket: function ( buffer )
    {
        return ParsePacket( buffer );
    },
    HEARTBEAT: 0x00,
    ACK: 0x01,
    POSITION: 0x02,
    INPUT: 0x03,
    SHOT: 0x04,
    LOBBYLIST: 0x05,
    JOINGAME: 0x06,
    ENDGAME: 0x07
}

// packet types     
var HEARTBEAT = 0x00;
var ACK = 0x01;
var POSITION = 0x02;
var INPUT = 0x03;
var SHOT = 0x04;
var LOBBYLIST = 0x05;
var JOINGAME = 0x06;
var ENDGAME = 0x07;

// packet sizes
var HEADER_SIZE = 8;
var POSITION_SIZE = HEADER_SIZE + 28;
var SHOT_SIZE = HEADER_SIZE + 4;
var ACK_SIZE = HEADER_SIZE + 4;
var LOBBYLIST_SIZE = HEADER_SIZE + 4;
var JOINGAME_SIZE = HEADER_SIZE + 4;
var ENDGAME_SIZE = HEADER_SIZE + 4;


//---------------------------------------------------------------------------
var ConstructPositionPacket = function ( channelID, packetID, playerID, rgba, position, velocity, turretDegrees )
{
    var buffer = new Buffer( POSITION_SIZE );
    var offset = 0;
    
    buffer.writeUInt8( channelID, offset ); // channel 1, ordered
    offset += 4;
    
    buffer.writeUInt32LE( packetID, offset );
    offset += 4;
    
    buffer.writeUInt8( POSITION, offset );
    offset += 1;
    
    buffer.writeUInt8( playerID, offset );
    offset += 1;
    
    buffer.writeUInt8( rgba.r, offset );
    offset += 1;
    
    buffer.writeUInt8( rgba.g, offset );
    offset += 1;
    
    buffer.writeFloatLE( position.x, offset );
    offset += 4;
    
    buffer.writeFloatLE( position.y, offset );
    offset += 4;
    
    buffer.writeFloatLE( velocity.x, offset );
    offset += 4;
    
    buffer.writeFloatLE( velocity.y, offset );
    offset += 4;
    
    buffer.writeFloatLE( turretDegrees, offset );
    offset += 4;
    
    buffer.writeUInt8( rgba.b, offset );
    
    return buffer;
}


//---------------------------------------------------------------------------
var ConstructShotPacket = function ( channelID, packetID, shootingPlayerID, didHit, hitPlayerID )
{
    var buffer = new Buffer( SHOT_SIZE );
    var offset = 0;

    buffer.writeUInt8( channelID, offset );
    offset += 4;

    buffer.writeUInt32LE( packetID, offset );
    offset += 4;

    buffer.writeUInt8( SHOT, offset );
    offset += 1;

    buffer.writeUInt8( shootingPlayerID, offset );
    offset += 1;

    buffer.writeUInt8( didHit, offset );
    offset += 1;

    buffer.writeUInt8( hitPlayerID, offset );
    offset += 1;

    return buffer;
}


//---------------------------------------------------------------------------
var ConstructAckPacket = function ( channelID, packetID, playerID )
{
    var buffer = new Buffer( ACK_SIZE );
    var offset = 0;
    
    buffer.writeUInt8( channelID, offset );
    offset += 4;
    buffer.writeUInt32LE( packetID, offset );
    offset += 4;
    buffer.writeUInt8( ACK, offset );
    offset += 1;
    buffer.writeUInt8( playerID, offset );
    return buffer;
}


//---------------------------------------------------------------------------
var ConstructLobbyListPacket = function ( channelID, packetID, numRooms )
{
    var buffer = new Buffer( LOBBYLIST_SIZE );
    var offset = 0;

    buffer.writeUInt8( channelID, offset );
    offset += 4;
    buffer.writeUInt32LE( packetID, offset );
    offset += 4;
    buffer.writeUInt8( LOBBYLIST, offset );
    offset += 1;
    buffer.writeUInt8( numRooms, offset );
    return buffer;
}


//---------------------------------------------------------------------------
var ConstructJoinGamePacket = function ( channelID, packetID, roomIndex, isNewGame )
{
    var buffer = new Buffer( JOINGAME_SIZE );
    var offset = 0;

    buffer.writeUInt8( channelID, offset );
    offset += 4;
    buffer.writeUInt32LE( packetID, offset );
    offset += 4;
    buffer.writeUInt8( JOINGAME, offset );
    offset += 1;
    buffer.writeUInt8( roomIndex, offset );
    offset += 1;
    buffer.writeUInt8( isNewGame, offset );
    return buffer;
}


//---------------------------------------------------------------------------
var ConstructEndGamePacket = function ( channelID, packetID )
{
    var buffer = new Buffer( ENDGAME_SIZE );
    var offset = 0;

    buffer.writeUInt8( channelID, offset );
    offset += 4;
    buffer.writeUInt32LE( packetID, offset );
    offset += 4;
    buffer.writeUInt8( ENDGAME, offset );
    return buffer;
}


//---------------------------------------------------------------------------
var ParsePacket = function ( buffer )
{
    var out = {};
    var offset = 0;
    try
    {
        if ( buffer.length < 9 )
        {
            console.log( "Invalid packet!" );
        }
        out.channelID = buffer.readUInt8( offset );
        offset += 4; // padding
        out.packetID = buffer.readUInt32LE( offset );
        offset += 4;
        out.packetType = buffer.readUInt8( offset );
        offset += 1;
        
        switch ( out.packetType )
        {
            case HEARTBEAT:
                {
                    out.playerID = buffer.readUInt8( offset );
                    break;
                }
            case ACK:
                {
                    out.playerID = buffer.readUInt8( offset );
                    break;
                }
            case POSITION:
                {
                    out.playerID = buffer.readUInt8( offset );
                    offset += 1;
                    out.r = buffer.readUInt8( offset );
                    offset += 1;
                    out.g = buffer.readUInt8( offset );
                    offset += 1;
                    out.x = buffer.readFloatLE( offset );
                    offset += 4;
                    out.y = buffer.readFloatLE( offset );
                    offset += 4;
                    out.velocityX = buffer.readFloatLE( offset );
                    offset += 4;
                    out.velocityY = buffer.readFloatLE( offset );
                    offset += 4;
                    out.turretDegrees = buffer.readFloatLE( offset );
                    offset += 4;
                    out.b = buffer.readUInt8( offset );
                    
                    break;
                }
            case INPUT:
                {
                    out.playerID = buffer.readUInt8( offset );
                    offset += 1;
                    out.inputBits = buffer.readUInt8( offset );
                    offset += 2;
                    out.directionX = buffer.readFloatLE( offset );
                    offset += 4;
                    out.directionY = buffer.readFloatLE( offset );
                    
                    break;
                }
            case JOINGAME:
                {
                    out.roomID = buffer.readUInt8( offset );
                    offset += 1;
                    out.isNewGame = buffer.readUInt8( offset );
                    break;
                }
            case ENDGAME:
                {
                    // nothing more to read
                    break;
                }
            default:
                {
                    out = null;
                    break;
                }
        }
        
        return out;
    }
    catch ( e )
    {
        console.log( e );
        out = null;
        return out
    }
}