﻿//---------------------------------------------------------------------------
// Lobby.js
//---------------------------------------------------------------------------

module.exports = function ()
{
    return new Lobby( );
}


var cmap = require( './cmap.js' );
var Session = require( './Session.js' );


/////////////////////////////////////////////////////////////////////////////
var Lobby = function()
{
    this.sessions = [];
    this.clientList = cmap.CMap( );
    this.currentRoomID = 0;

    //---------------------------------------------------------------------------
    this.NewSession = function ()
    {
        this.sessions.push( Session( this.currentRoomID ) );
        ++this.currentRoomID;
        if ( this.currentRoomID > 255 )
        {
            this.currentRoomID = 0;
        }
    }
    

    //---------------------------------------------------------------------------
    this.UpdateSessions = function ( playerUpdateFunc, sendEndGameFunc, sendLobbyListFunc, updateWinDataFunc )
    {
        for ( var index = 0; index < this.sessions.length; ++index )
        {
            this.sessions[ index ].UpdateSession( playerUpdateFunc, updateWinDataFunc );
            if ( this.sessions[ index ].isGameOver )
            {
                this.EndGame( index, sendEndGameFunc, sendLobbyListFunc );
            }
            //this.sessions[ index ].UpdatePlayers( playerUpdateFunc );
        }
    }
    
    
    //---------------------------------------------------------------------------
    this.GetSession = function ( roomID )
    {
        for ( var index = 0; index < this.sessions.length; ++index )
        {
            var session = this.sessions[ index ];
            if ( session.roomID == roomID ) return session;
        }
        return null;
    }
    
    
    //---------------------------------------------------------------------------
    this.SendPositionUpdates = function ( sendPositionFunc )
    {
        for ( var index = 0; index < this.sessions.length; ++index )
        {
            this.sessions[ index ].SendPositionUpdate( sendPositionFunc );
        }
    }


    //---------------------------------------------------------------------------
    this.AddPlayerToLobby = function ( key, value )
    {
        this.clientList.put( key, value );
    }
    
    
    //---------------------------------------------------------------------------
    this.LogPlayerIntoSession = function ( key, index, isNewSession, sendPositionFunc, sendLobbyListFunc )
    {
        var client = this.clientList.get( key );     
        if ( isNewSession )
        {
            this.NewSession( );
            this.sessions[ this.sessions.length - 1 ].AddPlayerToGame( key, client, sendPositionFunc );
            this.UpdateLobbyForLobbyPlayers( sendLobbyListFunc );
        }
        else
        {
            if ( this.sessions.length == 0 ) this.UpdateLobbyForLobbyPlayers( sendLobbyListFunc );
            if ( index >= this.sessions.length || index < 0 )
            {
                this.UpdateLobbyForLobbyPlayers( sendLobbyListFunc );
            }
            else
            {
                this.sessions[ index ].AddPlayerToGame( key, client, sendPositionFunc );
            }
        }
        console.log( "Player " + client.playerID + " joined session " + client.roomID );
        //this.sessions.get( roomID ).AddPlayerToGame( key, client );
    }
    
    
    //---------------------------------------------------------------------------
    this.UpdateLobbyForLobbyPlayers = function ( sendLobbyListFunc )
    {
        for ( var index = 0; index < this.clientList.size; ++index )
        {
            if ( this.clientList.value( ).roomID < 0 )
            {
                sendLobbyListFunc( this.clientList.value( ), false );
            }
            this.clientList.next( );
        }
    }
    
    
    //---------------------------------------------------------------------------
    this.DisconnectPlayer = function ( key, sendEndGameFunc, sendLobbyListFunc )
    {
        this.RemovePlayerFromSession( key, sendEndGameFunc, sendLobbyListFunc );
        this.clientList.remove( key );
    }
    
    
    //---------------------------------------------------------------------------
    this.RemovePlayerFromSession = function ( key, sendEndGameFunc, sendLobbyListFunc )
    {
        var client = this.clientList.get( key );
        var didGameEnd = false;
        client.score = 0;
        for ( var index = 0; index < this.sessions.length; ++index )
        {
            var s = this.sessions[ index ];
            if ( s.roomID == client.roomID )
            {
                this.sessions[ index ].RemovePlayerFromGame( key, sendEndGameFunc, sendLobbyListFunc );
                if ( this.sessions[ index ].clientList.size == 0 )
                {
                    this.EndGame( index, sendEndGameFunc, sendLobbyListFunc );
                    didGameEnd = true;
                }
                client.roomID = -1;
                break;
            }
        }
        return didGameEnd;
        //this.sessions.get( client.roomID ).RemovePlayerFromGame( key );
    }
    

    //---------------------------------------------------------------------------
    this.EndGame = function ( index, sendEndGameFunc, sendLobbyListFunc )
    {
        var s = this.sessions[ index ];
        console.log( "Game " + s.roomID + " ended!" );
        s.EndGame( sendEndGameFunc );
        this.sessions.splice( index, 1 );
        this.UpdateLobbyForLobbyPlayers( sendLobbyListFunc );
        //this.sessions.get( roomID ).EndGame( );
        //this.sessions.remove( roomID );
        //this.sessions[ roomID ].EndGame( );
    }
}