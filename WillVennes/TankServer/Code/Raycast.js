﻿//---------------------------------------------------------------------------
// Raycast.js
//---------------------------------------------------------------------------

module.exports = 
{
    CastRayAndGetResult: function ( startClient, rayLength, stepSize, session )
    {
        return new RaycastResult( startClient, rayLength, stepSize, session );
    }
}

var Vec2 = require( './Vec2.js' );
//var ClientInfo = require( './ClientInfo.js' );
//var CMap = require( './cmap.js' );


//---------------------------------------------------------------------------
var DegreesToRadians = function ( degrees )
{
    return degrees * ( Math.PI / 180 );
}


/////////////////////////////////////////////////////////////////////////////
var RaycastResult = function( startClient, rayLength, stepSize, session )
{
    this.startClient = startClient;
    this.currentPos = Vec2( startClient.position.x, startClient.position.y );
    this.direction = Vec2( Math.cos( DegreesToRadians( this.startClient.turretDegrees) ), Math.sin( DegreesToRadians( this.startClient.turretDegrees ) ) );
    this.maxLength = rayLength;
    this.currentLength = 0;
    this.stepSize = stepSize;
    this.hit = false;
    this.hitClient = null;
    this.session = session;
    
    this.direction.Multiply( stepSize );
    // step until collide or length has been met
    for ( this.currentLength = 0; this.currentLength <= this.maxLength; this.currentLength += this.stepSize )
    {
        if ( this.hit ) continue;
        this.currentPos.Add( this.direction );

        for ( var index = 0; index < this.session.clientList.size; ++index )
        {
            var currentClient = this.session.clientList.value( );
            
            var mins = Vec2( currentClient.position.x - 20, currentClient.position.y - 20 );
            var maxes = Vec2( currentClient.position.x + 20, currentClient.position.y + 20 );
            
            if ( !this.hit && currentClient != this.startClient &&
                 ( this.currentPos.x >= mins.x && this.currentPos.x <= maxes.x ) && 
                 ( this.currentPos.y >= mins.y && this.currentPos.y <= maxes.y ) )
            {
                this.hit = true;
                this.hitClient = currentClient;
            }

            this.session.clientList.next( );
        }
    }
}