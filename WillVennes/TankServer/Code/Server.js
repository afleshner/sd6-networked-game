﻿//---------------------------------------------------------------------------
// CaptureTheFlagServer.js
//---------------------------------------------------------------------------

// tools
var tools = require( './ServerTools.js' );

// game dimensions
var GAME_WINDOW_X = 1600;
var GAME_WINDOW_Y = 900;
var PLAYER_RADIUS = 20;
var FLAG_RADIUS = 20;
var TURRET_TURN_RATE = 90; // degrees per second
var MAX_SHOT_LENGTH = 400;

// timers
var TIME_TIL_DISCONNECT = 5000;
var TIME_TIL_UPDATE = 100;
var TIME_TIL_RESEND = 1000;
var TIME_TIL_RESHOOT = 500;
var DELTA_TIME = 1.0 / 60.0;
var TIME_POS_UPDATE = DELTA_TIME * 1000.0;

// input directions
var MOVEMENT_SPEED = 200;
var INPUT_TURRET_RIGHT = 0x01;
var INPUT_TURRET_LEFT = 0x02;
var INPUT_TURRET_SHOOT = 0x04;

// packet channels
var UNRELIABLE = 0;
var ORDERED = 1;
var RELIABLE = 2;

// input
var MAX_NUM_PACKETS_PROCESSED_PER_UPDATE = 500;

// server vars
var udpServer = tools.dgram.createSocket('udp4');
var udpPort;

// console input
var rl = tools.readline.createInterface(process.stdin, process.stdout, null);

// map for clients
var clientList = tools.cmap.CMap( );

// lobby
var lobby = tools.Lobby( );


//---------------------------------------------------------------------------
var SortPacketsByChannel = function( packet, sender )
{
    var client = clientList.get( sender.address + ":" + sender.port );
    switch ( packet.channelID )
    {
        default:
        case UNRELIABLE:
            {
                client.receivedUnreliablePacketQueue.push( packet );
                break;
            }
        case ORDERED:
            {
                client.receivedOrderedPacketQueue.Push( packet );
                break;
            }
        case RELIABLE:
            {
                client.receivedReliablePacketQueue.Push( packet );
                break;
            }
    }
}


//---------------------------------------------------------------------------
var ProcessAllReceivedPackets = function ()
{
    for ( var index = 0; index < clientList.size; ++ index )
    {
        var client = clientList.value( );
        ProcessReceivedPackets( client );
        clientList.next( );
    }
}


//---------------------------------------------------------------------------
var ProcessReceivedPackets = function ( client )
{
    ProcessUnreliablePackets( client );
    ProcessOrderedPackets( client );
    ProcessReliablePackets( client );
}


//---------------------------------------------------------------------------
var ProcessUnreliablePackets = function ( client )
{
    var packet;
    var count = 0;
    // unreliable
    while ( client.receivedUnreliablePacketQueue.length > 0 && count < MAX_NUM_PACKETS_PROCESSED_PER_UPDATE )
    {
        packet = client.receivedUnreliablePacketQueue.pop( );
        HandlePacket( packet, client );
        ++count;
    }
}


//---------------------------------------------------------------------------
var ProcessOrderedPackets = function ( client )
{
    var packet;
    var count = 0;
    // ordered
    while ( !client.receivedOrderedPacketQueue.IsEmpty( ) && count < MAX_NUM_PACKETS_PROCESSED_PER_UPDATE )
    {
        packet = client.receivedOrderedPacketQueue.At( 0 );
        
        if ( packet.packetID >= client.receivedOrderedPacketNumber )
        {
            HandlePacket( packet, client );
            
            client.receivedOrderedPacketNumber = 1 + packet.packetID;
        }
        
        client.receivedOrderedPacketQueue.Pop( );
        ++count;
    }
}


//---------------------------------------------------------------------------
var ProcessReliablePackets = function ( client )
{
    var packet;
    var count = 0;
    // reliable
    while ( !client.receivedReliablePacketQueue.IsEmpty( ) && count < MAX_NUM_PACKETS_PROCESSED_PER_UPDATE )
    {
        packet = client.receivedReliablePacketQueue.At( 0 );
        
        if ( client.receivedReliablePacketNumber == packet.packetID )
        {
            HandlePacket( packet, client );
            SendPacket( tools.PacketParser.ConstructAckPacket( 0, packet.packetID, client.playerID ), client ); // send ack          
            client.receivedReliablePacketQueue.Pop( );
            ++client.receivedReliablePacketNumber;
        }
        else if ( packet.packetID < client.receivedReliablePacketNumber )
        {
            // throw this packet away
            SendPacket( tools.PacketParser.ConstructAckPacket( 0, packet.packetID, client.playerID ), client ); // send ack
            client.receivedReliablePacketQueue.Pop( );
        }
        else
        {
            break;
        }
        ++count;
    }
}


//---------------------------------------------------------------------------
var HandlePacket = function ( packet, client ) 
{
    switch ( packet.packetType )
    {
        case tools.PacketParser.HEARTBEAT:
            {
                HandleHeartbeat(client);
                break;
            }
        case tools.PacketParser.ACK:
            {
                HandleAck(packet, client);
                break;
            }
        case tools.PacketParser.INPUT:
            {
                HandleInput(packet, client);
                break;
            }
        case tools.PacketParser.JOINGAME:
            {
                HandleJoinGame( packet, client );
                break;
            }
        case tools.PacketParser.ENDGAME:
            {
                HandleEndGame( packet, client );
                break;
            }
        default:
            {
                break;
            }
    }
}


//---------------------------------------------------------------------------
var HandleHeartbeat = function ( client ) 
{
    client.lifetime = Date.now();
}


//---------------------------------------------------------------------------
var HandleAck = function ( packet, client )
{
    client.lifetime = Date.now();
    
    // pop the top if packetID matches
    if ( !client.sentReliablePacketQueue.IsEmpty( ) && ( packet.packetID == client.sentReliablePacketQueue.At( 0 ).readUInt32LE( 4 ) ) )
    {
        console.log("Ack received for packet " + packet.packetID + " from client " + client.playerID);
        client.sentReliablePacketQueue.Pop();
        client.ackTimer = Date.now();
    }
    else if ( !client.sentReliablePacketQueue.IsEmpty( ) && ( packet.packetID > client.sentReliablePacketQueue.At( 0 ).readUInt32LE( 4 ) ) )
    {
        client.ackTimer = Date.now();
    }
}


//---------------------------------------------------------------------------
var HandleInput = function ( packet, client ) 
{
    client.lifetime = Date.now( );
    var direction = tools.Vec2( 0, 0 );
    var finalVelocity = tools.Vec2( 0, 0 );
    finalVelocity.Assign( client.velocity );
    
    var rightPressed = false;
    var leftPressed = false;
    var didShoot = false;
    var session = null;
    var currentTime = Date.now( );
    
    if ( ( packet.inputBits & INPUT_TURRET_RIGHT ) == INPUT_TURRET_RIGHT )
    {
        // clockwise
        client.turretSpeed = -TURRET_TURN_RATE;
        rightPressed = true;
    }
    
    if ( ( packet.inputBits & INPUT_TURRET_LEFT ) == INPUT_TURRET_LEFT )
    {
        // counter clockwise
        client.turretSpeed = TURRET_TURN_RATE;
        leftPressed = true;
    }
    
    if ( ( packet.inputBits & INPUT_TURRET_SHOOT ) == INPUT_TURRET_SHOOT )
    {
        if ( ( currentTime - client.timeSinceLastShot ) >= TIME_TIL_RESHOOT )
        {
            var session = lobby.GetSession( client.roomID );
            client.timeSinceLastShot = currentTime;
            didShoot = true;
        }
        else
        {
            didShoot = false;
        }
    }
    
    if ( packet.inputBits == 0 || ( rightPressed && leftPressed ) || ( !rightPressed && !leftPressed ) )
    {
        client.turretSpeed = 0;
    }

    direction.x = packet.directionX;
    direction.y = packet.directionY;
    
    direction.Normalize( );
    finalVelocity.Normalize( );
    finalVelocity.Multiply( 0.9 );
    //direction.Multiply( MOVEMENT_SPEED );
    direction.Multiply( 0.1 );
    finalVelocity.Add( direction );
    //finalVelocity.Normalize( );
    finalVelocity.Multiply( MOVEMENT_SPEED );
    
    //if ( ( packet.inputBits == 0 ) )
    if ( direction.x == 0 && direction.y == 0 )
    {
        finalVelocity.x = 0;
        finalVelocity.y = 0;
    }

    client.velocity.Assign( finalVelocity );

    // do the raycast
    if ( didShoot ) PerformRaycast( client, session );
}


//---------------------------------------------------------------------------
var PerformRaycast = function ( client, session )
{
    var packet = null;
    if ( session == null )
    {
        packet = tools.PacketParser.ConstructShotPacket( ORDERED, client.sentOrderedPacketNumber, client.playerID, false, 0 );
        SendOrderedPacket( packet, client );
        return;
    }

    var hit = tools.Raycast.CastRayAndGetResult( client, MAX_SHOT_LENGTH, PLAYER_RADIUS, session );
    
    if ( hit.hit )
    {
        console.log( "Player " + hit.startClient.playerID + " hit player " + hit.hitClient.playerID + "!" );
        ++client.score;
        hit.hitClient.position.x = Math.floor( Math.random( ) * GAME_WINDOW_X );
        hit.hitClient.position.y = Math.floor( Math.random( ) * GAME_WINDOW_Y );
    }

    session.SendShotPacket( SendShot, hit );
}


//---------------------------------------------------------------------------
var HandleJoinGame = function ( packet, client )
{
    client.lifetime = Date.now( );
    
    InitializeClient( client );
    lobby.LogPlayerIntoSession( client.ip + ":" + client.port, packet.roomID, packet.isNewGame, SendPosition, SendLobbyListToClient );
}


//---------------------------------------------------------------------------
var HandleEndGame = function ( packet, client )
{
    client.lifetime = Date.now( );
    var didGameEnd = lobby.RemovePlayerFromSession( client.ip + ":" + client.port, SendEndGameToClient, SendLobbyListToClient );
    if ( !didGameEnd )
    {
        SendEndGameToClient( client );
        SendLobbyListToClient( client, false );
    }
}


//---------------------------------------------------------------------------
var SendUnreliablePacket = function ( packet, receiver )
{
    packet.writeUInt8( 0, UNRELIABLE );
    SendPacket( packet, receiver );
}


//---------------------------------------------------------------------------
var SendOrderedPacket = function ( packet, receiver )
{
    packet.writeUInt8( ORDERED, 0 );
    packet.writeUInt32LE( receiver.sentOrderedPacketNumber, 4 );
    ++receiver.sentOrderedPacketNumber;
    SendPacket( packet, receiver );
}


//---------------------------------------------------------------------------
var SendReliablePacket = function ( packet, receiver )
{
    packet.writeUInt8( RELIABLE, 0 );
    packet.writeUInt32LE( receiver.sentReliablePacketNumber, 4 );
    ++receiver.sentReliablePacketNumber;
    receiver.sentReliablePacketQueue.Push( packet );
    SendPacket( packet, receiver );
    //console.log( "Sending reliable packet " + packet.readUInt32LE( 4 ) + " to player " + receiver.playerID );
    console.log( "Sending reliable packet " + receiver.sentReliablePacketQueue.At( 0 ).readUInt32LE( 4 ) + " to client " + receiver.playerID );
}


//---------------------------------------------------------------------------
var SendPosition = function ( sender, receiver )
{
    var packet = tools.PacketParser.ConstructPositionPacket(
        ORDERED,
        receiver.sentOrderedPacketNumber,
        sender.playerID,
        sender.color,
        sender.position,
        sender.velocity,
        sender.turretDegrees
    );
    //console.log( sender.turretDegrees + " " + packet.readFloatLE( 28 ) );
    SendOrderedPacket( packet, receiver );
    //++receiver.sentOrderedPacketNumber;

    //SendPacket( packet, receiver );
}


//---------------------------------------------------------------------------
var SendShot = function ( receiver, hitResult )
{
    //var channel = ORDERED;
    //var packetNumber = receiver.sentOrderedPacketNumber;
    //if ( hitResult.hit )
    //{
    //    channel = RELIABLE;
    //    packetNumber = receiver.sentReliablePacketNumber;
    //}
    var hitPlayerID = 0;
    if ( hitResult.hit ) hitPlayerID = hitResult.hitClient.playerID;
    var packet = tools.PacketParser.ConstructShotPacket(
        RELIABLE,
        receiver.sentReliablePacketNumber,
        hitResult.startClient.playerID,
        hitResult.hit,
        hitPlayerID
    );
    SendReliablePacket( packet, receiver );
}


//---------------------------------------------------------------------------
var InitializeClient = function ( clientInfo )
{
    clientInfo.lifetime = Date.now( );
    clientInfo.ackTimer = Date.now( );
    clientInfo.position.x = Math.floor( Math.random( ) * GAME_WINDOW_X );
    clientInfo.position.y = Math.floor( Math.random( ) * GAME_WINDOW_Y );
    clientInfo.velocity.x = 0;
    clientInfo.velocity.y = 0;
    clientInfo.turretDegrees = 0;
    clientInfo.timeSinceLastShot = Date.now( );
    clientInfo.color.r = Math.floor( Math.random( ) * 255 );
    clientInfo.color.g = Math.floor( Math.random( ) * 255 );
    clientInfo.color.b = Math.floor( Math.random( ) * 255 );
}


//---------------------------------------------------------------------------
var AddNewClient = function ( packet, remoteAddr ) 
{
    if ( packet == null || clientList.contains( remoteAddr.address + ":" + remoteAddr.port ) ) return;
    
    var newClient = tools.ClientInfo( );
    //InitializeClient( newClient );
    newClient.playerID = packet.playerID;
    newClient.ip = remoteAddr.address;
    newClient.port = remoteAddr.port;
    
    clientList.put(remoteAddr.address + ":" + remoteAddr.port, newClient);
    
    console.log( "Client " + newClient.playerID + " connected at " + remoteAddr.address + ":" + remoteAddr.port );
    
    lobby.AddPlayerToLobby( remoteAddr.address + ":" + remoteAddr.port, newClient );
    var lobbyPacket = tools.PacketParser.ConstructLobbyListPacket( 2, newClient.sentReliablePacketNumber, lobby.sessions.length );
    SendReliablePacket( lobbyPacket, newClient );

    tools.Leaderboard.InsertNewEntry( packet.playerID );
    //++newClient.sentReliablePacketNumber
    //newClient.sentReliablePacketQueue.Push( lobbyPacket );
    //SendPacket( lobbyPacket, newClient );
}


//---------------------------------------------------------------------------
var SetFlagLocation = function ( flagPosition ) 
{
    flagPosition.x = Math.floor(Math.random() * GAME_WINDOW_X);
    flagPosition.y = Math.floor(Math.random() * GAME_WINDOW_Y);
}


//---------------------------------------------------------------------------
var RemoveClient = function () 
{
    var time = Date.now();
    var disconnectedClients = [];
    
    for ( var i = 0; i < clientList.size; ++i )
    {
        if ( time - clientList.value( ).lifetime > TIME_TIL_DISCONNECT )
        {
            disconnectedClients.push( clientList.key( ) );
        }
        
        clientList.next();
    }
    
    if ( disconnectedClients.length > 0 )
    {
        for ( var j = 0; j < disconnectedClients.length; ++j )
        {
            var client = clientList.get( disconnectedClients[ j ] );
            console.log( "Client " + client.playerID + " at " + disconnectedClients[ j ] + " disconnected." );
            lobby.DisconnectPlayer( disconnectedClients[ j ], SendEndGameToClient, SendLobbyListToClient );
            clientList.remove( disconnectedClients[ j ] );
        }
    }
    
    setTimeout( RemoveClient, TIME_TIL_DISCONNECT );
}


//---------------------------------------------------------------------------
var UpdateClientPosition = function ( clientInfo, flagPosition, sessionPlayers )
{
    clientInfo.position.x += clientInfo.velocity.x * DELTA_TIME;
    clientInfo.position.y += clientInfo.velocity.y * DELTA_TIME;
    
    clientInfo.turretDegrees += clientInfo.turretSpeed * DELTA_TIME;
    
    if ( clientInfo.position.x + PLAYER_RADIUS > GAME_WINDOW_X ) clientInfo.position.x = GAME_WINDOW_X - PLAYER_RADIUS;
    if ( clientInfo.position.y + PLAYER_RADIUS > GAME_WINDOW_Y ) clientInfo.position.y = GAME_WINDOW_Y - PLAYER_RADIUS;
    if ( clientInfo.position.x - PLAYER_RADIUS < 0.0 ) clientInfo.position.x = PLAYER_RADIUS;
    if ( clientInfo.position.y - PLAYER_RADIUS < 0.0 ) clientInfo.position.y = PLAYER_RADIUS;
}


//---------------------------------------------------------------------------
var SendEndGameToClient = function ( clientInfo )
{
    var packet = tools.PacketParser.ConstructEndGamePacket( 2, clientInfo.sentReliablePacketNumber );
    SendReliablePacket( packet, clientInfo );
    //++clientInfo.sentReliablePacketNumber;
    //clientInfo.sentReliablePacketQueue.Push( packet );
    //SendPacket( packet, clientInfo );
}


//---------------------------------------------------------------------------
var SendLobbyListToClient = function ( clientInfo, isSplicing )
{
    var length = lobby.sessions.length;
    var packet;
    if ( isSplicing ) packet = tools.PacketParser.ConstructLobbyListPacket( 2, clientInfo.sentReliablePacketNumber, length - 1 );
    else packet = tools.PacketParser.ConstructLobbyListPacket( 2, clientInfo.sentReliablePacketNumber, length );
    SendReliablePacket( packet, clientInfo );
    //++clientInfo.sentReliablePacketNumber;
    //clientInfo.sentReliablePacketQueue.Push( packet );
    //SendPacket( packet, clientInfo );
}


//---------------------------------------------------------------------------
var Update = function () 
{
    //ProcessAllInputPackets();
    ProcessAllReceivedPackets( );
    lobby.UpdateSessions( UpdateClientPosition, SendEndGameToClient, SendLobbyListToClient, tools.Leaderboard.UpdateWinData );
    //console.log( "UpdateClientPositions" );
    setTimeout( Update, TIME_POS_UPDATE );
}


//---------------------------------------------------------------------------
var SendPositionUpdate = function () 
{
    for ( var i = 0; i < clientList.size; ++i )
    {
        var clientInfo = clientList.value();
        //var positionPacket = tools.PacketParser.ConstructPositionPacket( ORDERED, 0, clientInfo.playerID, clientInfo.color, clientInfo.position, clientInfo.velocity );
        
        //SendPositionToAllClients(positionPacket);
        
        //// send flag position
        //positionPacket = tools.PacketParser.ConstructPositionPacket( ORDERED, 0, 0, tools.Rgba( 255, 255, 255, 255 ), flagPosition, tools.Vec2( 0, 0 ) );
        //SendPositionToClient(positionPacket, clientInfo);
        
        // resend packet if not acknowledged
        if ( Date.now( ) - clientInfo.ackTimer > TIME_TIL_RESEND )
        {
            if ( !clientInfo.sentReliablePacketQueue.IsEmpty( ) )
            {
                console.log("Resending packet " + clientInfo.sentReliablePacketQueue.At(0).readUInt32LE(4) + " to client " + clientInfo.playerID);
                SendPacket(clientInfo.sentReliablePacketQueue.At(0), clientInfo);
            }
            
            clientInfo.ackTimer = Date.now();
        }
        
        clientList.next();
    }
    
    lobby.SendPositionUpdates( SendPosition );
    
    //console.log( "SendPositionUpdate" );
    setTimeout(SendPositionUpdate, TIME_TIL_UPDATE);
}


//---------------------------------------------------------------------------
var SendPacket = function ( msg, client ) 
{
    udpServer.send( msg, 0, msg.length, client.port, client.ip, function ( err, bytes ) 
    {
        if (err) throw err;
    });
}


//---------------------------------------------------------------------------
udpServer.on( 'listening', function () 
{
    var address = udpServer.address();
    console.log("UDP server listening on port " + address.port);
});


//---------------------------------------------------------------------------
udpServer.on( 'message', function ( msg, sender ) 
{
    //console.log( "Got message from " + sender.address + ":" + sender.port );
    
    var packet = tools.PacketParser.ParsePacket( msg );
    
    if ( packet != null )
    {
        AddNewClient( packet, sender );
        SortPacketsByChannel( packet, sender );
    }
    else
    {
        console.log( "Null packet!" );
        console.log( msg );
    }
    //console.log( "Type = " + packet.packetType + ", PlayerID = " + packet.playerID );
} );


//---------------------------------------------------------------------------
process.on( 'exit', function ()
{
    tools.Leaderboard.End( );
} );


//---------------------------------------------------------------------------
module.exports = 
{
    StartServer: function () {

        rl.question("Port: ", function (answer) {
            udpPort = answer;
            udpServer.bind(udpPort);
            tools.Leaderboard.Initialize( );
            tools.Leaderboard.CreateTable( );
            rl.close( );
        });
        
        //SetFlagLocation();
        
        setTimeout(RemoveClient, TIME_TIL_DISCONNECT);
        setTimeout(SendPositionUpdate, TIME_TIL_UPDATE);
        setTimeout(Update, TIME_POS_UPDATE);
    }
}