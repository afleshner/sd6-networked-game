﻿//---------------------------------------------------------------------------
// Leaderboard.js
//---------------------------------------------------------------------------

module.exports =
{
    Initialize: function () { Initialize( ); },
    End: function () { End( ); },
    CreateTable: function () { CreateTable( ); },
    InsertNewEntry: function ( playerID ) { InsertNewEntry( playerID ); },
    UpdateWinData: function( playerID ) { UpdateWinData( playerID ); }
}

var mysql = require( 'mysql' );
var connection = mysql.createConnection(
    {
        host        : 'mysql.gametheorylabs.com',
        user        : 'zadoke5_student',
        password    : 'guildhall',
        database    : 'zadoke5_smu'
    }
);


//---------------------------------------------------------------------------
var HandleDisconnect = function ()
{
    connection = mysql.createConnection( {
        host        : 'mysql.gametheorylabs.com',
        user        : 'zadoke5_student',
        password    : 'guildhall',
        database    : 'zadoke5_smu'
    } );
    
    connection.connect( function ( err )
    {
        if ( err )
        {
            console.log( "mysql connection error!" );
            setTimeout( HandleDisconnect, 2000 );
        }
    });

    connection.on( 'error', function ( err )
    {
        if ( !err.fatal ) return;
        console.log( "mysql error" );

        if ( err.code === 'PROTOCOL_CONNECTION_LOST' )
        {
            HandleDisconnect( );
        }
    } );
}


//---------------------------------------------------------------------------
var Initialize = function()
{
    HandleDisconnect( );
    //connection.connect( );
}


//---------------------------------------------------------------------------
var End = function()
{
    connection.end( );
}


//---------------------------------------------------------------------------
var CreateTable = function ()
{
    connection.query( 'CREATE TABLE IF NOT EXISTS leaderboard (id int(3) NOT NULL, numWins int(10) NOT NULL, UNIQUE INDEX id(id))', function ( err, rows, fields )
    {
        if ( err )
        {
            console.log( "Table exists." );
        }
        else
        {
            console.log( "Created table." );
        }
    });
}


//---------------------------------------------------------------------------
var InsertNewEntry = function ( playerID )
{
    var entry = 
    {
        id: playerID,
        numWins: 0
    }
    connection.query( 'INSERT IGNORE INTO leaderboard SET ?', entry, function ( err, result )
    {
        if ( err )
        {
            console.log( "Entry exists." );
        }
        else
        {
            console.log( "Inserting entry into leaderboard..." );
        }
    } );
}


//---------------------------------------------------------------------------
var UpdateWinData = function ( playerID )
{
    connection.query( "SELECT * from leaderboard WHERE id='" + playerID + "'", function ( err, rows, fields )
    {
        if ( err )
        {
            console.log( "Leaderboard data fetch error." );
        }
        else
        {
            var numWins = rows[0].numWins + 1;
            connection.query( "UPDATE leaderboard SET numWins='" + numWins + "' WHERE id='" + playerID + "'", function ( err, rows, fields )
            {
                if ( err )
                {
                    console.log( "Leaderboard data update error." );
                }
                else
                {
                    console.log( "Wins updated in database for player " + playerID );
                }
            } );
        }

    } );
}