﻿//---------------------------------------------------------------------------
// ClientInfo.js
//---------------------------------------------------------------------------


module.exports = function()
{
    return new ClientInfo( );
}

var Vec2 = require( './Vec2.js' );
var Rgba = require( './Rgba.js' );
var PacketQueue = require( './PacketQueue.js' );


/////////////////////////////////////////////////////////////////////////////
var ClientInfo = function ()
{
    this.playerID = 0;
    this.roomID = -1;
    this.score = 0;
    this.lifetime = 0;
    this.ackTimer = 0;
    this.sentReliablePacketNumber = 0;
    this.receivedReliablePacketNumber = 0;
    this.sentOrderedPacketNumber = 0;
    this.receivedOrderedPacketNumber = 0;
    this.position = Vec2( 0, 0 );
    this.velocity = Vec2( 0, 0 );
    this.turretDegrees = 90.0;
    this.turretSpeed = 0;
    this.timeSinceLastShot = 0;
    this.color = Rgba( 255, 255, 255, 255 );
    this.ip = "127.0.0.1";
    this.port = "5000"
    this.receivedUnreliablePacketQueue = [];
    this.sentReliablePacketQueue = PacketQueue( 1 );
    this.receivedReliablePacketQueue = PacketQueue( 0 );
    this.receivedOrderedPacketQueue = PacketQueue( 0 );
}