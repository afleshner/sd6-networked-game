﻿//---------------------------------------------------------------------------
// Session.js
//---------------------------------------------------------------------------

module.exports = function( roomID )
{
    return new Session( roomID );
}

var cmap = require( './cmap.js' );
var Vec2 = require( './Vec2.js' );


var MAX_SCORE = 5;


/////////////////////////////////////////////////////////////////////////////
var Session = function( roomID )
{
    this.roomID = roomID;
    this.clientList = cmap.CMap( );
    this.flagPosition = Vec2( Math.floor( Math.random( ) * 1600 ), Math.floor( Math.random( ) * 900 ) );
    this.isGameOver = false;
    this.owningClient = null;

    //---------------------------------------------------------------------------
    this.AddPlayerToGame = function ( key, value, sendPositionFunc )
    {
        value.roomID = this.roomID;
        if ( this.clientList.size == 0 )
        {
            this.owningClient = value;
        }
        this.clientList.put( key, value );
        this.SendPositionUpdate( sendPositionFunc );
    }
    

    //---------------------------------------------------------------------------
    this.RemovePlayerFromGame = function ( key, sendEndGameFunc, sendLobbyListFunc )
    {
        var client = this.clientList.get( key );
        if ( client == this.owningClient )
        {
            // end the game if this is the owning client
            this.EndGame( sendEndGameFunc, sendLobbyListFunc );
        }
        else
        {
            client.roomID = -1; // LOBBY
            this.clientList.remove( key );
        }
    }
    
    
    //---------------------------------------------------------------------------
    this.EndGame = function ( sendEndGameFunc )
    {
        for ( var index = 0; index < this.clientList.size; ++index )
        {
            sendEndGameFunc( this.clientList.value( ) );
            this.clientList.value( ).roomID = -1; // LOBBY
            //this.RemovePlayerFromGame( this.clientList.key( ) );
            this.clientList.next( );
        }
        this.clientList.removeAll( );
    }
    

    //---------------------------------------------------------------------------
    this.UpdateSession = function ( playerUpdateFunc, updateWinDataFunc )
    {
        for ( var index = 0; index < this.clientList.size; ++index )
        {
            var client = this.clientList.value( );
            
            if ( !this.isGameOver )
            {
                playerUpdateFunc( client, this.flagPosition, this.clientList );
                
                if ( client.score >= MAX_SCORE )
                {
                    client.score = 0;
                    console.log( "Game " + this.roomID + " finished!" );
                    this.isGameOver = true;
                    updateWinDataFunc( client.playerID );
                //endGameFunc( this.roomID, sendEndGameFunc );
                }
            }
            this.clientList.next( );
        }
    }


    //---------------------------------------------------------------------------
    this.SendPositionUpdate = function ( sendPositionFunc )
    {
        for ( var i = 0; i < this.clientList.size; ++i )
        {
            var sender = this.clientList.value( );

            for ( var j = 0; j < this.clientList.size; ++j )
            {
                var receiver = this.clientList.value( );
                
                sendPositionFunc( sender, receiver );

                this.clientList.next( );
            }
            this.clientList.next( );
        }
    }


    //---------------------------------------------------------------------------
    this.SendShotPacket = function ( sendPacketFunc, hitResult )
    {
        for ( var index = 0; index < this.clientList.size; ++index )
        {
            var receiver = this.clientList.value( );
            sendPacketFunc( receiver, hitResult );
            this.clientList.next( );
        }
    }
}