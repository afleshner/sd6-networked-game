﻿//---------------------------------------------------------------------------
// Vec2.js
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
module.exports = function( x, y )
{
    return new Vec2( x, y );
}


/////////////////////////////////////////////////////////////////////////////
var Vec2 = function( x, y )
{
    this.x = x;
    this.y = y;

    //---------------------------------------------------------------------------
    this.Assign = function ( rhs )
    {
        this.x = rhs.x;
        this.y = rhs.y;
    }
    
    
    //---------------------------------------------------------------------------
    this.Add = function ( rhs )
    {
        this.x += rhs.x;
        this.y += rhs.y;
    }
    
    
    //---------------------------------------------------------------------------
    this.Subtract = function ( rhs )
    {
        this.x -= rhs.x;
        this.y -= rhs.y;
    }
    
    
    //---------------------------------------------------------------------------
    this.Multiply = function ( rhs )
    {
        this.x *= rhs;
        this.y *= rhs;
    }
    
    
    //---------------------------------------------------------------------------
    this.Divide = function ( rhs )
    {
        var oneOverRhs = 1.0 / rhs;
        this.Multiply( oneOverRhs );
    }
    
    
    //---------------------------------------------------------------------------
    this.DistanceSquared = function ()
    {
        var result = this.x * this.x + this.y * this.y;
        return result;
    }
    
    
    //---------------------------------------------------------------------------
    this.Distance = function ()
    {
        var result = Math.sqrt( this.DistanceSquared( ) );
        return result;
    }
    
    
    //---------------------------------------------------------------------------
    this.Normalize = function ()
    {
        var length = this.Distance( );
        if ( length != 0 ) this.Divide( length );
    }
}