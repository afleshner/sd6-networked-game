﻿//---------------------------------------------------------------------------
// PacketQueue.js
//---------------------------------------------------------------------------


module.exports = function( isBinary )
{
    return new PacketQueue( isBinary );
}


/////////////////////////////////////////////////////////////////////////////
var PacketQueue = function( isBinary )
{
    this.isBinary = isBinary;
    this.packetQueue = [];
    
    //---------------------------------------------------------------------------
    this.Push = function( obj )
    {
        this.packetQueue.push( obj );
        
        if ( this.isBinary )
        {
            this.packetQueue.sort( function ( a, b )
            {
                return a.readUInt32LE( 4 ) - b.readUInt32LE( 4 );
            } );
        }
        else
        {
            this.packetQueue.sort( function ( a, b )
            {
                return a.packetID - b.packetID;
            } );
        }
    }

    //---------------------------------------------------------------------------
    this.Pop = function( obj )
    {
        this.packetQueue.shift( );
    }

    //---------------------------------------------------------------------------
    this.At = function( index )
    {
        return this.packetQueue[ index ];
    }

    //---------------------------------------------------------------------------
    this.Length = function ()
    {
        return this.packetQueue.length;
    }

    //---------------------------------------------------------------------------
    this.IsEmpty = function ()
    {
        return this.packetQueue.length == 0;
    }
}