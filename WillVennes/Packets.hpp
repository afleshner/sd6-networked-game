//---------------------------------------------------------------------------
// Packets.hpp
//---------------------------------------------------------------------------


#pragma once
#ifndef _INCLUDED_PACKETS_
#define _INCLUDED_PACKETS_

#include <queue>
#include <vector>

#include "Engine/Math/Colors.hpp"
#include "Engine/Math/Vec2.hpp"

#define MAX_PACKET_SIZE 512


/////////////////////////////////////////////////////////////////////////////
union HeartbeatPacket
{
	struct Heartbeat
	{
		unsigned char channelID; // make sure to take the padding into account
		unsigned int packetID;
		unsigned char packetType; // 0
		unsigned char playerID;
	} h;

	char buffer[ sizeof( Heartbeat ) ];
};


/////////////////////////////////////////////////////////////////////////////
union AcknowledgePacket
{
	struct Ack
	{
		unsigned char channelID;
		unsigned int packetID;
		unsigned char packetType; // 1
		unsigned char playerID;
	} ack;

	char buffer[ sizeof( Ack ) ];
};


/////////////////////////////////////////////////////////////////////////////
union PositionPacket
{
	struct posColor
	{
		unsigned char channelID;
		// PADDING
		// PADDING
		// PADDING
		unsigned int packetID;
		unsigned char packetType; // 2
		unsigned char playerID; // 0 = flag
		unsigned char r;
		unsigned char g;
		float x;
		float y;
		float velocityX;
		float velocityY;
		float turretDegrees;
		unsigned char b;
	} pos;

	char buffer[ sizeof( posColor ) ];
};


/////////////////////////////////////////////////////////////////////////////
static unsigned char NORTH = 1;
static unsigned char SOUTH = 2;
static unsigned char EAST = 4;
static unsigned char WEST = 8;
union InputPacket
{
	struct Input
	{
		unsigned char channelID;
		// PADDING
		// PADDING
		// PADDING
		unsigned int packetID;
		unsigned char packetType; // 3
		unsigned char playerID;
		unsigned char inputBits; // 1 = turret right, 2 = turret left, 4 = fire
		// PADDING
		float directionX;
		float directionY;
	} input;

	char buffer[ sizeof( Input ) ];
};


/////////////////////////////////////////////////////////////////////////////
union ShotPacket
{
	struct Shot
	{
		unsigned char channelID;
		unsigned int packetID;
		unsigned char packetType; // 4
		unsigned char shootingPlayerID;
		unsigned char didHit; // 0 for false
		unsigned char hitPlayerID;
	} shot;

	char buffer[ sizeof( Shot ) ];
};


/////////////////////////////////////////////////////////////////////////////
union LobbyListPacket
{
	struct Lobby
	{
		unsigned char channelID;
		unsigned int packetID;
		unsigned char packetType; // 5
		unsigned char numRooms;
	} lobby;

	char buffer[ sizeof( Lobby ) ];
};


/////////////////////////////////////////////////////////////////////////////
union JoinGamePacket
{
	struct JoinGame
	{
		unsigned char channelID;
		unsigned int packetID;
		unsigned char packetType; // 6
		unsigned char roomIndex;
		unsigned char isNewGame; // 0 for false, anything else for true
	} join;

	char buffer[ sizeof( JoinGame ) ];
};


/////////////////////////////////////////////////////////////////////////////
union EndGamePacket
{
	struct EndGame
	{
		unsigned char channelID;
		unsigned int packetID;
		unsigned char packetType; // 7
	} end;

	char buffer[ sizeof( EndGame ) ];
};

#endif