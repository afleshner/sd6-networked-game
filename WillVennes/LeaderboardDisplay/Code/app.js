﻿//---------------------------------------------------------------------------
// app.js
//---------------------------------------------------------------------------


var mysql = require( 'mysql' );
var readline = require( 'readline' );
var rl = readline.createInterface( process.stdin, process.stdout, null );


var connection = mysql.createConnection(
    {
        host        : 'mysql.gametheorylabs.com',
        user        : 'zadoke5_student',
        password    : 'guildhall',
        database    : 'zadoke5_smu'
    }
);


//---------------------------------------------------------------------------
var SortByNumWins = function( rows )
{
    if ( rows == null ) return;
    rows.sort( function ( a, b )
    {
        return b.numWins - a.numWins;
    } );
}


//---------------------------------------------------------------------------
var GetTableFromServer = function()
{
    connection.query( "SELECT * from leaderboard", function ( err, rows, fields )
    {
        if ( err )
        {
            console.log( "Table error!" );
        }
        else
        {
            SortByNumWins( rows );
            PrintRows( rows );
        }
    } );
}


//---------------------------------------------------------------------------
var PrintRows = function ( rows )
{
    console.log( "### Leaderboard ###" );
    for ( var index = 0; index < rows.length; ++index )
    {
        console.log( "-- Player ID: " + rows[ index ].id + " Wins: " + rows[index].numWins );
    }

    rl.question( "Press enter to exit...", function ( answer )
    {
        process.exit( );
    } );
}


//---------------------------------------------------------------------------
var Startup = function()
{
    connection.connect( );
    GetTableFromServer( );
    connection.end( );
}


//---------------------------------------------------------------------------
Startup( );